﻿using RoboRyanTron.Unite2017.Variables;
using UnityEngine;
using UnityEngine.UI;

public class FuelBar : MonoBehaviour
{
    public FloatReference FuelLeft;
    public bool IsForHomeUser;
    
    private Slider slider;

    private void Awake()
    {
        slider = GetComponentInChildren<Slider>();
        slider.direction = IsForHomeUser ? Slider.Direction.LeftToRight : Slider.Direction.RightToLeft;
    }

    private void Update()
    {
        slider.value = FuelLeft.Value;
    }
}
