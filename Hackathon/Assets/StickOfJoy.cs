﻿using System.Collections;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Variables;
using UnityEngine;

public class StickOfJoy : MonoBehaviour
{
    // Start is called before the first frame update
    public float valX, valY;
    public float treshold=0.2f;
    public Joystick joystick;

    public FloatReference Horizontal;
    public FloatReference Vertical;

    // Update is called once per frame
    void Update()
    {
        valX = Mathf.Abs(joystick.Horizontal) > treshold ? joystick.Horizontal : 0;
        valY = Mathf.Abs(joystick.Vertical) > treshold ? joystick.Vertical : 0;

        Horizontal.Variable.Value = valX;
        Vertical.Variable.Value = valY;

    }
}
