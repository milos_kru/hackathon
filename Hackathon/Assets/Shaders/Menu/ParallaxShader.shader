﻿Shader "Custom/ParallaxShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_DepthTex ("Depth", 2D) = "white" {}
		_Intensity("Intensity", Range(0, 0.1)) = 0.1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform sampler2D _DepthTex;
			uniform float4 _DepthTex_ST;
			uniform fixed2 _Pivot;
			uniform float _Intensity;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed depth_shade = tex2D(_DepthTex, i.uv).r;
				fixed2 discplacement = _Pivot * _Intensity * (depth_shade * 2 - 1);
				fixed4 col = tex2D(_MainTex, i.uv - discplacement);
				return col * col.a;
			}
			ENDCG
		}
	}
}
