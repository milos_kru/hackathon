﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GlowControl : MonoBehaviour
{
    public Material mSharedMaterial;
    [SerializeField] private float glowSpeed = 0.005f;
    private float _currentValue = 0;

    /* Author: Strahinja Janjic (Ubivam)
     * This script every is called every frame
     * and it takse really small incremet "glowSpeed"
     * and apply it to a current value of GlowPower property
     * of a TextMesh Pro Material.
     */
    public void Update()
    {
        if (mSharedMaterial != null)
        {
            _currentValue += glowSpeed;
            if (_currentValue > 0.5f || _currentValue < 0f)
            {
                glowSpeed *= -1;
            }

            mSharedMaterial.SetFloat(ShaderUtilities.ID_GlowPower, _currentValue);
        }
        else
        {
            Debug.LogError("Font Material is not loaded!");
        }
    }
}