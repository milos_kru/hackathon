﻿using System;
using Menu;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenuScript : MonoBehaviour
{
    public AudioMixer audioMixer;

    public Slider volumeSlider;

    public void Start()
    {
        volumeSlider.value = PlayerSettingsModule.GetMasterVolume();
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("MasterVolume", volume);
    }

    public void SaveVolume()
    {
        PlayerSettingsModule.SetMasterVolume(volumeSlider.value);
    }
}