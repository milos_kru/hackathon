﻿using UnityEngine;

[ExecuteInEditMode]
public class MouseTrackingScript : MonoBehaviour {

	private void Update () {
        float pivotX = (Input.mousePosition.x / Screen.width) * 2 - 1;
        float pivotY = (Input.mousePosition.y / Screen.height) * 2 - 1;
        Shader.SetGlobalVector("_Pivot", new Vector2(pivotX, pivotY));
    }

}
