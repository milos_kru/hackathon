﻿using System;
using RoboRyanTron.Unite2017.Variables;
using TMPro;
using UnityEngine;

public class GameHub : MonoBehaviour
{
    public TextMeshProUGUI VictimText;
    public FloatReference VictimsSaved;
    
    private int victimCount = -1;

    private void Update()
    {
        var count = (int) VictimsSaved.Value;
        if (victimCount != count)
        {
            victimCount = count;
            VictimText.text = $"Count: {victimCount}";
        }
    }
}
