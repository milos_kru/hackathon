﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    private LevelChange _levelChange;
    private float waitTimeSecounds;

    public void Awake()
    {
        _levelChange = FindObjectOfType<LevelChange>();
    }

    public void Play()
    {
        if (_levelChange != null)
        {
            _levelChange.LoadLevel(1);
        }
        else
        {
            SceneManager.LoadScene(1);
        }
    }
 
    public void Exit()
    {
        Application.Quit();
    }
}