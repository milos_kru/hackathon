﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject pauseButton;
    public GameObject gameHub;

    private LevelChange _levelChange;

    private bool isPaused = false;

    private void Awake()
    {
        _levelChange = FindObjectOfType<LevelChange>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Pause()
    {
        pauseMenu.SetActive(true);
        pauseButton.SetActive(false);
        gameHub.SetActive(false);
        Time.timeScale = 0.0f;
        isPaused = true;
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        pauseButton.SetActive(true);
        gameHub.SetActive(true);
        
        Time.timeScale = 1.0f;
        isPaused = false;
    }

    public void MainMenu()
    {
        Time.timeScale = 1.0f;
        if (_levelChange != null)
        {
            _levelChange.LoadLevel(0);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

}
