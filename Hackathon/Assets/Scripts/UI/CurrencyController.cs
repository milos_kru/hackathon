﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

public class CurrencyController : MonoBehaviour
{
    public TextMeshProUGUI tm;

    void Start()
    {
        Refresh();
    }

    // Update is called once per frame
    void Update()
    {
        Refresh();
    }

    public void AddToCurrency(int amount)
    {
        GameModel.Instance.AddSoftCurrency(amount);
        Refresh();
    }

    private void Refresh()
    {
        tm.SetText(GameModel.Instance.SoftCurrency.ToString());
    }
}