﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelChange : MonoBehaviour
{
    [SerializeField]
    private Animator anim;

    private int _levelIndex;    
    
    private static readonly int kLevelChange = Animator.StringToHash("levelChange");

    public void LoadLevel(int levelIndex)
    {
        //Start fade transition, note event must be attached to animation
        anim.SetTrigger(kLevelChange);
        _levelIndex = levelIndex;
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(_levelIndex);
    }
    
}
