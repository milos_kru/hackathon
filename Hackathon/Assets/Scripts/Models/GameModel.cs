using System;
using System.Collections.Generic;
using Menu;
using UnityEngine;

public class GameModel
{
    #region Singleton

    private static GameModel instance;

    public static GameModel Instance => instance ?? (instance = new GameModel());
    
    #endregion

    #region Initialization

    private const string GAME_MODEL_KEY = "game_model_data";
    
    private GameModel()
    {
        if (PlayerSettingsModule.ContainsKey(GAME_MODEL_KEY))
        {
            var json = PlayerSettingsModule.GetString(GAME_MODEL_KEY);
            Data = JsonUtility.FromJson<GameModelData>(json);
        }
        else
        {
            Data = new GameModelData();
        }
    }

    public void Save()
    {
        var json = JsonUtility.ToJson(Data, true);
        PlayerSettingsModule.SaveString(GAME_MODEL_KEY, json);   
    }

    #endregion

    #region Fields

    public GameModelData Data;

    #endregion

    #region Properties

    public int SoftCurrency => Data.SoftCurrency;

    #endregion

    public void AddSoftCurrency(int amount)
    {
        Data.SoftCurrency += amount;
    }

    public void GameFinished(int score)
    {
        Data.SoftCurrency += Mathf.CeilToInt(GameSettings.Instance.ScoreToCurrency.Evaluate(score));
    }
}

[Serializable]
public class GameModelData
{
    public List<ShipData> AllShips;

    public int SoftCurrency;
}