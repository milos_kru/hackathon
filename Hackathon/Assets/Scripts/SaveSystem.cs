﻿using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void OnApplicationQuit()
    {
        GameModel.Instance.Save();
    }
}
