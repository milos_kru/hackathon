using System.IO;
using UnityEditor;
using UnityEngine;

public class GameSettings : ScriptableObject
{
    public const string FolderPath = "Assets/Resources";

    private static GameSettings instance;

    public static GameSettings Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<GameSettings>(typeof(GameSettings).Name);

#if UNITY_EDITOR
                if (instance == null)
                {
                    GameSettings asset = CreateInstance<GameSettings>();
                    AssetDatabase.CreateAsset(asset, Path.Combine(FolderPath, typeof(GameSettings).Name + ".asset"));
                    AssetDatabase.SaveAssets();
                }
#endif
            }

            return instance;
        }
    }

    public float IntroStartDelay;
    public float IntroDuration;
    public float OutroDuration;
    
    public AnimationCurve ScoreToCurrency;
    public float FuelDepletionPerSecond;
}
