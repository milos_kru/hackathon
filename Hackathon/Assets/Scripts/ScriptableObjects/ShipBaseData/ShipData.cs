using System;
using System.Collections.Generic;

[Serializable]
public class ShipData
{
    public List<int> LevelPerAttribute;
    public ShipBaseData BaseData;

    public float GetPropertyValue(ShipPropertyType type)
    {
        var level = LevelPerAttribute[(int) type];
        return BaseData.GetValue(type, level);
    }
}