﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewShipBaseData", menuName = "Data/ShipBaseData")]
public class ShipBaseData : ScriptableObject
{
    public ShipType Type;
    
    public List<ShipLevelUpVariable> Values;
    public GameObject Prefab;
    
    public float GetValue(ShipPropertyType type, int level)
    {
        var index = Values.FindIndex(data => data.Type == type);

        if (index == -1)
        {
            Debug.LogError("Error");
            return 0f;
        }

        var levelUpVariable = Values[index];
        return levelUpVariable.GetValue(level);
    }

    public GameObject InstantiatePrefab()
    {
        return Instantiate(Prefab);
    }
}

[Serializable]
public enum ShipPropertyType
{
    MaxSpeed = 0,
    MaxCapacity = 1,
    MaxFuel = 2,
}

