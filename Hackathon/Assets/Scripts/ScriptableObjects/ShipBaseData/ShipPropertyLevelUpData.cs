﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewShipPropertyLevelUpData", menuName = "Data/ShipPropertyLevelUpData")]
public class ShipPropertyLevelUpData : ScriptableObject
{
    public List<float> ValuesPerLevel;

    public float GetValue(int level)
    {
        if (ValuesPerLevel == null || ValuesPerLevel.Count == 0 || ValuesPerLevel.Count < level)
        {
            Debug.LogError("List is empty");
            return 0f;
        }

        return ValuesPerLevel[level];
    }
}
