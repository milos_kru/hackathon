﻿using System;

[Serializable]
public struct ShipPropertyVariable
{
    public ShipPropertyType Type;
    public int Level;
}

[Serializable]
public struct ShipLevelUpVariable
{
    public ShipPropertyType Type;
    public ShipPropertyLevelUpData LevelUpData;

    public float GetValue(int level) => LevelUpData.GetValue(level);
}