using UnityEngine;

[CreateAssetMenu(fileName = "NewVictimBaseData", menuName = "Data/VictimBaseData")]

public class VictimData : ScriptableObject
{
    public int Score;
    public int SoftCurrencyReward;

    public GameObject Prefab;
    public GameObject InstantiatePrefab()
    {
        return Instantiate(Prefab);
    }
}
