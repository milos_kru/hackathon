using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ShipDatabase : ScriptableObject
{
    public const string FolderPath = "Assets/Resources";

    private static ShipDatabase instance;

    public static ShipDatabase Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<ShipDatabase>(typeof(ShipDatabase).Name);

#if UNITY_EDITOR
                if (instance == null)
                {
                    ShipDatabase asset = CreateInstance<ShipDatabase>();
                    AssetDatabase.CreateAsset(asset, Path.Combine(FolderPath, typeof(ShipDatabase).Name + ".asset"));
                    AssetDatabase.SaveAssets();
                }
#endif
            }

            return instance;
        }

    }

    public List<ShipBaseData> ShipTypes;
    public List<VictimData> VictimTypes;
    
    public int ShipPropetiesNumberOfLevels = 10;
}

public enum ShipType
{
    Ship0 = 0,
    Ship1 = 1,
    Ship2 = 2,
    Ship3 = 3,
    Ship4 = 4,
}