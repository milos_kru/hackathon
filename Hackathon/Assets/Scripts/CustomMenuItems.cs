
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public static class CustomMenuItems
{
    [MenuItem ("Custom/Clear Player Prefs")]
    private static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("Cleared player prefs");
    }

    [MenuItem("Custom/Refresh Ship Database")]
    private static void RefreshShipDatabase()
    {
        var allShips = GetAllInstances<ShipBaseData>();
        allShips.Sort((s1, s2) => s1.Type.CompareTo(s2.Type));

        var allVictimData = GetAllInstances<VictimData>();

        ShipDatabase.Instance.VictimTypes = allVictimData;
    }

    [MenuItem ("Custom/Create Ship Property files")]
    private static void CreateShipPropertyFiles()
    {
        RefreshShipDatabase();

        var ships = ShipDatabase.Instance.ShipTypes;

        foreach (var ship in ships)
        {
            var path = AssetDatabase.GetAssetPath(ship);
            var directory = Path.GetDirectoryName(path);
            var propertyDirectory = Path.Combine(directory, "Properties");
            Debug.Log(propertyDirectory);
            if (!Directory.Exists(propertyDirectory))
            {
                Directory.CreateDirectory(propertyDirectory);
            }
            
            ship.Values.Clear();

            foreach (ShipPropertyType type in Enum.GetValues(typeof(ShipPropertyType)))
            {
                var propertyPath = Path.Combine(propertyDirectory, type + ".asset");
                if (!File.Exists(propertyPath))
                {
                    var asset = ScriptableObject.CreateInstance<ShipPropertyLevelUpData>();
                    asset.ValuesPerLevel = Enumerable.Repeat(0f, ShipDatabase.Instance.ShipPropetiesNumberOfLevels)
                        .ToList();
                    AssetDatabase.CreateAsset(asset, propertyPath);
                }

                var property = AssetDatabase.LoadAssetAtPath<ShipPropertyLevelUpData>(propertyPath);

                ship.Values.Add(new ShipLevelUpVariable
                {
                    Type = type,
                    LevelUpData = property
                });
            }
            
            EditorUtility.SetDirty(ship);
        }

        AssetDatabase.SaveAssets();
    }
    
    public static List<T> GetAllInstances<T>() where T : ScriptableObject
    {
        var guids = AssetDatabase.FindAssets("t:"+ typeof(T).Name);  //FindAssets uses tags check documentation for more info
        var allInstances = new List<T>(guids.Length);
        for (int i = 0; i < guids.Length; i++) //probably could get optimized 
        {
            var path = AssetDatabase.GUIDToAssetPath(guids[i]);
            allInstances.Add(AssetDatabase.LoadAssetAtPath<T>(path));
        }

        return allInstances;
    }
}