﻿using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    // TODO: move this to player settings according to boat and its upgrades
    [Header("Demo Boat Settings")]
    public float BoatMaxSpeed;
    public float MovementLerpFactor;
    public float RotationLerpFactor;
    public float VortexPullForce;
    public float VortexPullLerpFactor;

    private Vector3 velocity;
    private Rigidbody physicsBody;
    private Transform childTransform;

    private void Start()
    {
        physicsBody = GetComponent<Rigidbody>();
        childTransform = transform.GetChild(0);
    }

    private void FixedUpdate()
    {
        var inputVector = GetInputVector();
        UpdateVelocity(inputVector);
        RotateTowardsVelocity();
        PerformMovement();
    }

    private Vector3 GetInputVector()
    {
        var inputHorizontal = GameManager.Instance.InputController.horizontal;
        var inputVertical = GameManager.Instance.InputController.vertical;
        return new Vector3(inputHorizontal, 0, inputVertical).normalized;
    }

    private void UpdateVelocity(Vector3 direction)
    {
        velocity = Vector3.Lerp(velocity, Quaternion.AngleAxis(45, Vector3.up) * direction * BoatMaxSpeed, MovementLerpFactor);
    }

    private void RotateTowardsVelocity()
    {
        if (velocity.normalized != Vector3.zero)
        {
            childTransform.rotation = Quaternion.Lerp(childTransform.rotation, Quaternion.LookRotation(velocity.normalized, Vector3.up), RotationLerpFactor);
        }
    }

    private void PerformMovement()
    {
        physicsBody.MovePosition(gameObject.transform.position + velocity * Time.fixedDeltaTime);
    }

    public void PullTowards(Vector3 pullForcePosition)
    {
        var direction = pullForcePosition - gameObject.transform.position;
        velocity = Vector3.Lerp(velocity, Quaternion.AngleAxis(45, Vector3.up) * direction * VortexPullForce, VortexPullLerpFactor);
    }

}
