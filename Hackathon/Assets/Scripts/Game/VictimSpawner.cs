﻿using System.Collections;
using UnityEngine;

public class VictimSpawner : MonoBehaviour
{
    public Transform[] FixedSpawnPositions;
    public GameObject VictimPrefab;
    public int InitialSpawnCount;
    public float SpawnPeriodSeconds;
    public float RetryCount;

    public GameObject[] liveVictims;

    private IEnumerator Start()
    {
        liveVictims = new GameObject[FixedSpawnPositions.Length];

        for (int i = 0; i < InitialSpawnCount; ++i)
        {
            SpawnVictim();
        }

        while (true)
        {
            SpawnVictim();
            yield return new WaitForSeconds(SpawnPeriodSeconds);
        }
    }

    private void SpawnVictim()
    {
        for (int j = 0; j < RetryCount; ++j)
        {
            int index = Random.Range(0, liveVictims.Length - 1);
            if (liveVictims[index] == null)
            {
                liveVictims[index] = Instantiate(VictimPrefab, FixedSpawnPositions[index]);
                Debug.Log("Victim Spawned!");
                break;
            }
        }
    }

}
