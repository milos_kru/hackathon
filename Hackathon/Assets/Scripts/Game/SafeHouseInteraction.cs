﻿using UnityEngine;

public class SafeHouseInteraction : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            GameManager.Instance.OnSafeHouseReached();
        }
    }

}
