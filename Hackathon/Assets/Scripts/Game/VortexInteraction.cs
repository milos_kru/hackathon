﻿using UnityEngine;

public class VortexInteraction : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            Debug.Log("ENTERED");
            other.gameObject.GetComponentInParent<PlayerControls>().PullTowards(transform.position);
        }
    }

}
