﻿using UnityEngine;

public class VictimInteraction : MonoBehaviour
{
    private VictimData VictimData;
    
    private void Start()
    {
        VictimData = ShipDatabase.Instance.VictimTypes[Random.Range(0, ShipDatabase.Instance.VictimTypes.Count - 1)];
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("ADASDASDASDA");
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            Debug.Log("QEQWEQEQWEQWE");
            GameManager.Instance.OnVictimCollected(VictimData);

            Destroy(gameObject);
        }
    }

}
