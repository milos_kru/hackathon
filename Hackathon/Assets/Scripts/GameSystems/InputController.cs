﻿using RoboRyanTron.Unite2017.Variables;
using UnityEngine;

/*Author: Strahinja Janjic
* This class is central module for all inputs inside of a game
* all outer classes should comunicate direclty to this class to get and Input
*/
public class InputController : MonoBehaviour
{
    [HideInInspector] public float vertical;
    [HideInInspector] public float horizontal;
    [HideInInspector] public Vector2 mouseInput;
    [HideInInspector] public bool Fire1;
    public Joystick joystick;
    public float treshold = 0.2f;

    public FloatReference Horizontal;
    public FloatReference Vertical;

    private void Update()
    {
        if(joystick!=null)
        {
            horizontal = (Mathf.Abs(joystick.Horizontal) > treshold) ? joystick.Horizontal : 0;
            vertical = (Mathf.Abs(joystick.Vertical) > treshold) ? joystick.Vertical : 0;
        }
        else
        {
            vertical = Input.GetAxisRaw("Vertical");
            horizontal = Input.GetAxisRaw("Horizontal");
        }


        //horizontal = (Mathf.Abs(joystick.Horizontal) > treshold) ? joystick.Horizontal : 0;
        //vertical = (Mathf.Abs(joystick.Vertical) > treshold) ? joystick.Vertical : 0;
        horizontal = Horizontal.Value;
        vertical = Vertical.Value;
        // mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        //  Fire1 = Input.GetButton("Fire1");
        
    }
}