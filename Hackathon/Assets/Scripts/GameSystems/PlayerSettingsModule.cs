﻿using UnityEngine;

/* Author: Strahinja Janjc(Ubivam)
 * Player Settings Module is a wrapper class around PlayerPrefs
 * witch centralises access to this UnityClass and prevents key errors
 */
namespace Menu
{
    public static class PlayerSettingsModule
    {
        private const string MASTER_VOLUME_KEY = "master_volume";
        private const string DIFFICULTY_KEY = "difficulty";
        private const string CURRENCY_KEY = "currency";
        private const string LEVEL_KEY = "level_unlocked_";

        public static void SaveString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
            PlayerPrefs.Save();
        }

        public static string GetString(string key) => PlayerPrefs.GetString(key);

        public static bool ContainsKey(string key) => PlayerPrefs.HasKey(key);

        public static void setCurrencyAmout(int amount)
        {
            PlayerPrefs.SetInt(CURRENCY_KEY,amount);
        }

        public static int GetCurrencyAmout()
        {
            return PlayerPrefs.GetInt(CURRENCY_KEY);
        }

        public static void AddCurrencyAmount(int amount)
        {
            var currentCurrency = PlayerPrefs.GetInt(CURRENCY_KEY);
            currentCurrency += amount;
            PlayerPrefs.SetInt(CURRENCY_KEY,currentCurrency);
        }

        public static void SetMasterVolume(float volume)
        {
            if (volume >= 0f && volume <= 1f)
            {
                PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, volume);
            }
            else
            {
                Debug.LogError("Master volume out of range");
            }
        }

        public static float GetMasterVolume()
        {
            return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
        }


        public static void UnlockLevel(int level)
        {
            if (level <= Application.levelCount - 1)
            {
                PlayerPrefs.SetInt(LEVEL_KEY + level.ToString(), 1); // Use 1 for true
            }
            else
            {
                Debug.LogError("Trying to unlock level not in build order");
            }
        }

        public static bool IsLevelUnlocked(int level)
        {
            int levelValue = PlayerPrefs.GetInt(LEVEL_KEY + level.ToString());
            bool isLevelUnlocked = (levelValue == 1);

            if (level <= Application.levelCount - 1)
            {
                return isLevelUnlocked;
            }
            else
            {
                Debug.LogError("Trying to query level not in build order");
                return false;
            }
        }


        public static void SetDifficulty(float difficulty)
        {
            if (difficulty >= 1f && difficulty <= 3f)
            {
                PlayerPrefs.SetFloat(DIFFICULTY_KEY, difficulty);
            }
            else
            {
                Debug.LogError("Difficulty out of range");
            }
        }

        public static float GetDifficulty()
        {
            return PlayerPrefs.GetFloat(DIFFICULTY_KEY);
        }
    }
}