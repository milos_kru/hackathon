﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RoboRyanTron.Unite2017.Variables;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Properties
    public static GameManager Instance { get; private set; }
    public InputController InputController { get; private set; }
    
    #endregion

    #region Fields

    private readonly List<VictimData> victimsInBoat = new List<VictimData>();
    private readonly List<VictimData> victimsSaved = new List<VictimData>();

    private bool isGameFinished;
    private bool IsTimerActive;

    public FloatReference FuelLeft;
    public FloatReference GameScore;
    public FloatReference VictimsSaved;
    
    #endregion
    
    #region Events
	
    public Action OnGameIntroStarted;
    public Action OnGameIntroFinished;
    public Action OnGameStarted;
    public Action OnGameFinished;
    public Action OnGameOutroStarted;
    public Action OnGameOutroFinished;
	
    #endregion
    
    
    private void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        
        InputController = GetComponent<InputController>();

        VictimsSaved.Variable.Value = 0; 
        FuelLeft.Variable.Value = 1f; //todo read from ship data
        GameScore.Variable.Value = 0f;
    }

    private void Start()
    {

        isGameFinished = false;
        IsTimerActive = false;
        
        StartCoroutine(GameFlow());
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    private void Update()
    {
        if (IsTimerActive)
        {
            FuelLeft.Variable.Value -= GameSettings.Instance.FuelDepletionPerSecond * Time.deltaTime;
            if (FuelLeft <= 0f)
            {
                FuelLeft.Variable.Value = 0f;
                IsTimerActive = false;
                
                NotifyGameFinished();
            }
        }
    }

    private IEnumerator GameFlow()
    {
        var gameSettings = GameSettings.Instance;

        yield return new WaitForSecondsRealtime(gameSettings.IntroStartDelay);
		
        NotifyGameIntroStarted();
        
        yield return new WaitForSecondsRealtime(gameSettings.IntroDuration);
		
        NotifyGameIntroFinished();
		
        yield return null;
		
        NotifyGameStarted();

        yield return new WaitUntil(() => isGameFinished);
		
        NotifyGameFinished();
        
        yield return null;

        NotifyGameOutroStarted();		
        
        yield return new WaitForSecondsRealtime(gameSettings.OutroDuration);
		
        NotifyGameOutroFinished();
    }

    #region Private

    private void NotifyGameIntroStarted()
    {
        OnGameIntroStarted?.Invoke();
    }

    private void NotifyGameIntroFinished()
    {
        OnGameIntroFinished?.Invoke();
    }

    private void NotifyGameStarted()
    {
       
        
        OnGameStarted?.Invoke();
    }

    private void NotifyGameFinished()
    {
        OnGameFinished?.Invoke();


        var score = victimsSaved.Sum(data => data.Score);
        GameModel.Instance.GameFinished(score);
    }

    private void NotifyGameOutroStarted()
    {
        OnGameOutroStarted?.Invoke();
    }

    private void NotifyGameOutroFinished()
    {
        OnGameOutroFinished?.Invoke();
    }
    
    #endregion

    public void OnVictimCollected(VictimData victim)
    {
        victimsInBoat.Add(victim);
    }

    public void OnSafeHouseReached()
    {
        foreach (var victimData in victimsInBoat)
        {
            GameScore.Variable.ApplyChange(victimData.Score);
        }

        victimsSaved.AddRange(victimsInBoat);
        victimsInBoat.Clear();
        
        // TODO: increment victimsSaved according to victimsOnBoat
        // TODO: increment fuel according to victimsOnBOat
    }
}

public static class GameManagerUtils
{
}