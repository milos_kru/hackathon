﻿Shader "Hero Olympics/Decal" {
	
	Properties {
		_DecalTex ("Decal", 2D) = "white" {}
		_Transparency ("Transparency", Range(0, 1)) = 1
	}
	
	Subshader {
		
		Tags { 
			"RenderType"="Transparent" 
			"Queue"="Transparent"
		}
		
		ZWrite Off
		ColorMask RGB	
		Blend SrcAlpha OneMinusSrcAlpha
		Offset -1, -1

		Pass {

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			struct v2f {
				float4 uvDecal : TEXCOORD0;
				float4 pos : SV_POSITION;
				float3 world : TEXCOORD1;
			};
			
			uniform float4x4 unity_Projector;
			uniform sampler2D _DecalTex;
			uniform float _Transparency;
			
			v2f vert (float4 vertex : POSITION) {
				v2f o;
				o.pos = UnityObjectToClipPos(vertex);
				o.uvDecal = mul(unity_Projector, vertex);
				o.world = mul(unity_ObjectToWorld, vertex).xyz;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target {
				fixed4 col = tex2Dproj(_DecalTex, UNITY_PROJ_COORD(i.uvDecal));
				fixed alpha = saturate(col.a * _Transparency);
				return fixed4(col.rgb, alpha);
			}
			ENDCG
		}
	}
}
