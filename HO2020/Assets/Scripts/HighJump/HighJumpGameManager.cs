﻿using System.Collections;
using UnityEngine;
using System;

namespace Olympics {
	public class HighJumpGameManager : GameManager {

		#region Enum
		public enum State {
			INTRO,
			GAME,
			OUTRO,
			UNDEFINED
		}
		#endregion

		#region Events
		public event Action OnIntroStarted;
		public event Action OnIntroFinished;
		public event Action OnGameStarted;
		public event Action OnGameFinished;
		public event Action OnOutroStarted;
		public event Action OnOutroFinished;
		#endregion

		#region Fields
		public State CurrentState { get; private set; }
		#endregion

		#region Mono
		private void Awake() {
			Initialize();
		}

		private void Start() {
			StartCoroutine(GameFlow());
		}
		#endregion

		#region Public
		public void GameFinished() {
			CurrentState = State.UNDEFINED;
		}
		#endregion

		#region Protected
		protected override IEnumerator GameFlow() {
			var sharedSettings = GameSettings.SharedSetings;

			yield return new WaitForSecondsRealtime(sharedSettings.IntroOutroSettings.IntroStartDelay);
			NotifyIntroStarted();
			SetCurrentState(State.INTRO);

			yield return new WaitForSecondsRealtime(sharedSettings.IntroOutroSettings.IntroDuration);
			NotifyIntroFinished();
			SetCurrentState(State.UNDEFINED);

			yield return null;
			NotifyGameStarted();
			SetCurrentState(State.GAME);

			yield return new WaitUntil(() => CurrentState != State.GAME);
			NotifyGameFinished();
			SetCurrentState(State.UNDEFINED);

			yield return null;
			NotifyOutroStarted();
			SetCurrentState(State.OUTRO);

			yield return new WaitForSecondsRealtime(sharedSettings.IntroOutroSettings.OutroDuration);
			NotifyOutroFinished();
			SetCurrentState(State.UNDEFINED);
		}
		#endregion

		#region Private
		private void Initialize() {
			CurrentState = State.UNDEFINED;
		}

		private void SetCurrentState(State state) {
			CurrentState = state;
		}

		private void NotifyIntroStarted() {
			OnIntroStarted?.Invoke();
		}

		private void NotifyIntroFinished() {
			OnIntroFinished?.Invoke();
		}

		private void NotifyGameStarted() {
			OnGameStarted?.Invoke();
		}

		private void NotifyGameFinished() {
			OnGameFinished?.Invoke();
		}

		private void NotifyOutroStarted() {
			OnOutroStarted?.Invoke();
		}

		private void NotifyOutroFinished() {
			OnOutroFinished?.Invoke();
		}
		#endregion
	}
}
