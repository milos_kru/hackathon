﻿namespace Olympics {
	public class HighJumpPlayer : Player {

		#region Fields
		public PlayerSettings Settings { get; private set; }
		#endregion

		#region Mono
		private void Awake() {
			Settings = PlayerSettings;
		}
		#endregion
	}
}
