﻿using UnityEngine;

namespace Olympics {
	[RequireComponent(typeof(BoxCollider))]
	public abstract class Pad : MonoBehaviour {

		#region Fields
		protected GameSettings GameSettings { get; private set; }
		protected Player Player { get; private set; }

		private bool actionTaken;
		private BoxCollider boxCollider;
		#endregion
		
		#region Mono
		private void Awake() {
			Initialize();
		}

		private void OnTriggerEnter(Collider other) {
			if (IsPlayer(other.gameObject)) {
				actionTaken = false;
			}
		}

		private void OnTriggerStay(Collider other) {
			if (ShouldPlayerAccelerate(other.gameObject)) {
				var offset = CalculateActionValue(other);
				TakeAction(offset);
				actionTaken = true;
			}
		}
		#endregion

		#region Abstract
		protected abstract void TakeAction(float offset);
		#endregion

		#region Private
		private void Initialize() {
			GameSettings = GameManager.GameSettings;
			Player = GameManager.Player;
			boxCollider = GetComponent<BoxCollider>();
		}

		private float CalculateActionValue(Collider other) {
			var playerWorldPosition = other.gameObject.transform.position;
			var playerLocalPosition = transform.worldToLocalMatrix.MultiplyPoint(playerWorldPosition);
			return playerLocalPosition.z + boxCollider.center.z + boxCollider.size.z * 0.5f;
		}

		private bool IsPlayer(GameObject obj) {
			return obj.layer == LayerMask.NameToLayer("Player");
		}

		private bool ShouldPlayerAccelerate(GameObject obj) {
			return IsPlayer(obj) && GameManager.InputController.Tap && !actionTaken;
		}
		#endregion
	}
}
