﻿using System.Collections;
using UnityEngine;

namespace Olympics {
	public abstract class GameManager : MonoBehaviour {

		#region Static
		private static GameManager instance;

		public static GameManager Instance {
			get {
				if (instance == null) {
					instance = Load();
				}
				return instance;
			}
		}

		private static GameSettings gameSettings;
		public static GameSettings GameSettings {
			get {
				if (gameSettings == null) {
					gameSettings = GameSettings.Load();
				}
				return gameSettings;
			}
		}

		private static InputController inputController;
		public static InputController InputController {
			get {
				if (inputController == null) {
					inputController = InputController.Load();
				}
				return inputController;
			}
		}

		private static Player player;
		public static Player Player {
			get {
				if (player == null) {
					player = Player.Load();
				}
				return player;
			}
		}

		private static GameManager Load() {
			return FindObjectOfType<GameManager>();
		}
		#endregion

		#region Field
		public Discipline Discipline { get; private set; }
		#endregion

		#region Abstract
		protected abstract IEnumerator GameFlow();
		#endregion

		#region Protected
		protected void BaseInitialize() {
			Discipline = GetDiscipline();
		}
		#endregion

		#region Private
		// TODO: Not working! sometimes...
		private Discipline GetDiscipline() {
			if (GetType() == typeof(HighJumpGameManager)) {
				return Discipline.HIGH_JUMP;
			} else if (GetType() == typeof(HummerThrowGameManager)) {
				return Discipline.HUMMER_THROW;
			} else if (GetType() == typeof(HurdlingGameManager)) {
				return Discipline.HURDLING;
			} else if (GetType() == typeof(JavelinThrowGameManager)) {
				return Discipline.JAVELIN_THROW;
			} else if (GetType() == typeof(LongJumpGameManager)) {
				return Discipline.LONG_JUMP;
			} else if (GetType() == typeof(RunningGameManager)) {
				return Discipline.RUNNING;
			} else {
				Debug.LogError("GameManager type unknown!");
				return Discipline.UNDEFINED;
			}
		}
		#endregion
	}
}
