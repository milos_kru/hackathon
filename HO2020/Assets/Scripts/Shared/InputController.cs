﻿using UnityEngine;

namespace Olympics {
	public class InputController : MonoBehaviour {

		#region Static
		public static InputController Load() {
			return FindObjectOfType<InputController>();
		}
		#endregion

		#region Fields
		[HideInInspector]
		public bool Tap { get; private set; }
		#endregion

		#region Mono
		private void Update() {
			Tap = Input.GetKeyDown(KeyCode.Mouse0);
		}
		#endregion
	}
}
