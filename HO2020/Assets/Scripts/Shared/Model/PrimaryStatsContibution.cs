﻿using System;
using UnityEngine;

namespace Olympics {

	#region Classes
	[Serializable]
	public class PrimaryStatsContibution {

		#region Static
		public static PrimaryStatsContibution GetPrimaryStatsContibutionByDiscipline(Discipline discipline) {
			var gameSettings = GameManager.GameSettings;
			switch (discipline) {
				case Discipline.HIGH_JUMP:
					return gameSettings.HighJumpGameSettings.PrimaryStatsContibution;
				case Discipline.HUMMER_THROW:
					return gameSettings.HammerThrowGameSettings.PrimaryStatsContibution;
				case Discipline.HURDLING:
					return gameSettings.HurdlingGameSettings.PrimaryStatsContibution;
				case Discipline.JAVELIN_THROW:
					return gameSettings.JavelinThrowGameSettings.PrimaryStatsContibution;
				case Discipline.LONG_JUMP:
					return gameSettings.LongJumpGameSettings.PrimaryStatsContibution;
				case Discipline.RUNNING:
					return gameSettings.RunningGameSettings.PrimaryStatsContibution;
				default:
					Debug.LogError("Player type unknown!");
					return null;
			}
		}
		#endregion

		#region Properties
		[Range(0f, 1f)]
		public float PowerContibution;
		[Range(0f, 1f)]
		public float SpeedContibution;
		[Range(0f, 1f)]
		public float TehniqueContibution;
		#endregion
	}
	#endregion
}