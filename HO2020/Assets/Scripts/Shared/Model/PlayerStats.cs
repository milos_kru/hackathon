﻿using System;
using UnityEngine;

namespace Olympics {

	#region Classes
	[Serializable]
	public class PrimaryStats {

		#region Properties
		public float Power;
		public float Speed;
		public float Tehnique;
		#endregion

		#region Public
		public float GetSumByContribution(PrimaryStatsContibution primaryStatsContibution) {
			var power = primaryStatsContibution.PowerContibution * Power;
			var speed = primaryStatsContibution.SpeedContibution * Speed;
			var tehnique = primaryStatsContibution.TehniqueContibution * Tehnique;
			return power + speed + tehnique;
		}
		#endregion
	}

	[Serializable]
	public class SecondaryStats {

		#region Properties
		[Range(0f, 1f)]
		public float HammerThrowBoost;
		[Range(0f, 1f)]
		public float HighJumpBoost;
		[Range(0f, 1f)]
		public float HurdlingBoost;
		[Range(0f, 1f)]
		public float JavelinThrowBoost;
		[Range(0f, 1f)]
		public float LongJumpBoost;
		[Range(0f, 1f)]
		public float RunningBoost;
		#endregion

		#region Public
		public float GetSecondaryStatByDiscipline(Discipline discipline) {
			switch (discipline) {
				case Discipline.HIGH_JUMP:
					return HighJumpBoost;
				case Discipline.HUMMER_THROW:
					return HammerThrowBoost;
				case Discipline.HURDLING:
					return HurdlingBoost;
				case Discipline.JAVELIN_THROW:
					return JavelinThrowBoost;
				case Discipline.LONG_JUMP:
					return LongJumpBoost;
				case Discipline.RUNNING:
					return RunningBoost;
				default:
					Debug.LogError("Player type unknown!");
					return 0f;
			}
		}
		#endregion
	}
	#endregion
}
