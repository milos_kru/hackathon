﻿using System;
using UnityEngine;

namespace Olympics {

	#region Classes
	[Serializable]
	public class HighJumpGameplaySettings {
	}

	[Serializable]
	public class HummerThrowGameplaySettings {
	}

	[Serializable]
	public class HurdlingGameplaySettings {

		#region Properties
		public float InitialMaxSpeed;
		public float Acceleration;
		public AnimationCurve AccelerationLayout = new AnimationCurve(
														new Keyframe(0f, 0.2f, 0f, 0f),
														new Keyframe(0.5f, 1f, 0f, 0f),
														new Keyframe(1f, 0.2f, 0f, 0f));
		public AnimationCurve JumpingLayout = new AnimationCurve(
														new Keyframe(0f, 0.2f, 0f, 0f),
														new Keyframe(1f, 1f, 0f, 0f));
		#endregion
	}

	[Serializable]
	public class JavelinThrowGameplaySettings {
	}

	[Serializable]
	public class LongJumpGameplaySettings {
	}

	[Serializable]
	public class RunningGameplaySettings {
	}
	#endregion
}
