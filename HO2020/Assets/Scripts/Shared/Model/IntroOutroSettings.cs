﻿using System;

namespace Olympics {

	#region Classes
	[Serializable]
	public class IntroOutroSettings {

		#region Properties
		public float IntroStartDelay;
		public float IntroDuration;
		public float OutroDuration;
		#endregion
	}
	#endregion
}