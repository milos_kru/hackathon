﻿using System;

namespace Olympics {

	#region Enum
	[Serializable]
	public enum Discipline {
		HIGH_JUMP,
		HUMMER_THROW,
		HURDLING,
		JAVELIN_THROW,
		LONG_JUMP,
		RUNNING,
		UNDEFINED
	}
	#endregion
}
