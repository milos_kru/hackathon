﻿using System;

namespace Olympics {

	#region Classes
	[Serializable]
	public class HighJumpGameSettings {

		#region Properties
		public PrimaryStatsContibution PrimaryStatsContibution;
		public HighJumpGameplaySettings HighJumpGameplaySettings;
		#endregion
	}

	[Serializable]
	public class HummerThrowGameSettings {

		#region Properties
		public PrimaryStatsContibution PrimaryStatsContibution;
		public HummerThrowGameplaySettings HammerThrowGameplaySettings;
		#endregion
	}

	[Serializable]
	public class HurdlingGameSettings {

		#region Properties
		public PrimaryStatsContibution PrimaryStatsContibution;
		public HurdlingGameplaySettings HurdlingGameplaySettings;
		#endregion
	}

	[Serializable]
	public class JavelinThrowGameSettings {

		#region Properties
		public PrimaryStatsContibution PrimaryStatsContibution;
		public JavelinThrowGameplaySettings JavelinThrowGameplaySettings;
		#endregion
	}

	[Serializable]
	public class LongJumpGameSettings {

		#region Properties
		public PrimaryStatsContibution PrimaryStatsContibution;
		public LongJumpGameplaySettings LongJumpGameplaySettings;
		#endregion
	}

	[Serializable]
	public class RunningGameSettings {

		#region Properties
		public PrimaryStatsContibution PrimaryStatsContibution;
		public RunningGameplaySettings RunningGameplaySettings;
		#endregion
	}
	#endregion
}
