﻿using UnityEngine;

namespace Olympics {
	public abstract class FinishLine : MonoBehaviour {

		#region Mono
		private void Awake() {
			Initialize();
		}

		private void OnTriggerEnter(Collider other) {
			if (IsPlayer(other.gameObject)) {
				TakeAction();
			}
		}
		#endregion

		#region Fields
		protected Player Player { get; private set; }
		#endregion

		#region Abstract
		protected abstract void TakeAction();
		#endregion

		#region Private
		private void Initialize() {
			Player = GameManager.Player;
		}

		private bool IsPlayer(GameObject obj) {
			return obj.layer == LayerMask.NameToLayer("Player");
		}
		#endregion
	}
}
