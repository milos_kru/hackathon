﻿using UnityEngine;

namespace Olympics {
	[CreateAssetMenu(menuName = "Hero Olympics/Game Settings", fileName = "GameSettings")]
	public class GameSettings : ScriptableObject {

		#region Static
		public static GameSettings Load() {
			return Resources.Load<GameSettings>(typeof(GameSettings).Name);
		}
		#endregion

		#region Properties
		public SharedSettings SharedSetings;
		public HummerThrowGameSettings HammerThrowGameSettings;
		public HighJumpGameSettings HighJumpGameSettings;
		public HurdlingGameSettings HurdlingGameSettings;
		public JavelinThrowGameSettings JavelinThrowGameSettings;
		public LongJumpGameSettings LongJumpGameSettings;
		public RunningGameSettings RunningGameSettings;
		#endregion
	}
}
