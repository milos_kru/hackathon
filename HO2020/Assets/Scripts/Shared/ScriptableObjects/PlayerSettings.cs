﻿using UnityEngine;

namespace Olympics {
	[CreateAssetMenu(menuName = "Hero Olympics/Player Settings", fileName = "PlayerSettings")]
	public class PlayerSettings : ScriptableObject {
		
		#region Static
		public static PlayerSettings Load(Discipline discipline) {
			var prefix = GetDisciplinePrefix(discipline);
			Debug.Log(prefix);
			return Resources.Load<PlayerSettings>(prefix + typeof(PlayerSettings).Name);
		}

		private static string GetDisciplinePrefix(Discipline discipline) {
			switch (discipline) {
				case Discipline.HIGH_JUMP:
					return "HighJump";
				case Discipline.HUMMER_THROW:
					return "HammerThrow";
				case Discipline.HURDLING:
					return "Hurdling";
				case Discipline.JAVELIN_THROW:
					return "JavelinThrow";
				case Discipline.LONG_JUMP:
					return "LongJump";
				case Discipline.RUNNING:
					return "Running";
				default:
					Debug.LogError("Player type unknown!");
					return "";
			}
		}
		#endregion

		#region Properties
		public PrimaryStats PrimaryStats;
		public SecondaryStats SecondaryStats;
		#endregion
	}
}
