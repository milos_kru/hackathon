﻿using UnityEngine;

namespace Olympics {
	public abstract class Player : MonoBehaviour {

		#region Static
		public static Player Load() {
			return FindObjectOfType<Player>();
		}
		#endregion

		#region Fields
		protected GameManager GameManager { get; private set; }
		protected GameSettings GameSettings { get; private set; }
		protected PlayerSettings PlayerSettings { get; private set; }
		protected float PlayerPerformance { get; private set; }
		#endregion

		#region Protected
		protected void BaseInitialize() {
			GameManager = GameManager.Instance;
			GameSettings = GameManager.GameSettings;
			var discipline = GameManager.Discipline;
			PlayerSettings = PlayerSettings.Load(discipline);
			PlayerPerformance = CalculatePlayerPerformanceByDiscipline(discipline);
		}
		#endregion

		#region Private
		private float CalculatePlayerPerformanceByDiscipline(Discipline discipline) {
			var secondaryStats = PlayerSettings.SecondaryStats;
			var secondaryStat = secondaryStats.GetSecondaryStatByDiscipline(discipline);
			var primaryStatsContibution = PrimaryStatsContibution.GetPrimaryStatsContibutionByDiscipline(discipline);
			var primaryStats = PlayerSettings.PrimaryStats;
			var sum = primaryStats.GetSumByContribution(primaryStatsContibution);
			return (1f + secondaryStat) * sum;
		}
		#endregion
	}
}
