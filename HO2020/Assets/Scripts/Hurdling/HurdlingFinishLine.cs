﻿namespace Olympics {
	public class HurdlingFinishLine : FinishLine {

		#region Protected
		protected override void TakeAction() {
			((HurdlingPlayer) Player).Finished();
		}
		#endregion
	}
}
