﻿namespace Olympics {
	public class HurdlingJumpingPad : Pad {

		#region Protected
		protected override void TakeAction(float offset) {
			var gameplaySettings = GameSettings.HurdlingGameSettings.HurdlingGameplaySettings;
			var amount = gameplaySettings.JumpingLayout.Evaluate(offset);
			((HurdlingPlayer) Player).Jump(amount);
		}
		#endregion
	}
}
