﻿using UnityEngine;

namespace Olympics {
	[RequireComponent(typeof(Rigidbody))]
	public class HurdlingPlayer : Player {

		#region Fields
		private Rigidbody body;
		private float currentSpeed;
		private float maxSpeed;
		private float acceleration;
		#endregion

		#region Mono
		private void Awake() {
			Initialize();
		}

		private void FixedUpdate() {
			var state = ((HurdlingGameManager) GameManager).CurrentState;
			switch (state) {
				case HurdlingGameManager.State.GAME:
					Move();
					return;
				default:
					return;
			}
			
		}
		#endregion
		
		#region Public
		public void Accelerate(float amount) {
			Debug.Log("Accelerated! " + amount);
			maxSpeed += amount;
		}
		
		public void Jump(float amount) {
			Debug.Log("Jumped! " + amount);
			body.AddForce(transform.up * 50 * amount, ForceMode.Impulse);
		}
		
		public void Finished() {
			Debug.Log("Finished!");
			((HurdlingGameManager) GameManager).GameFinished();
		}
		#endregion
		
		#region Private
		private void Initialize() {
			BaseInitialize();
			var gameplaySettings = GameSettings.HurdlingGameSettings.HurdlingGameplaySettings;
			maxSpeed = gameplaySettings.InitialMaxSpeed;
			acceleration = gameplaySettings.Acceleration;
			body = GetComponent<Rigidbody>();
		}

		private void Move() {
			currentSpeed += acceleration * Time.fixedDeltaTime;
			currentSpeed = Mathf.Min(currentSpeed, maxSpeed);
			body.MovePosition(body.position + new Vector3(-currentSpeed, 0, 0) * Time.fixedDeltaTime);
		}
		#endregion
	}
}
