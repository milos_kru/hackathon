﻿namespace Olympics {
	public class HurdlingAccelerationPad : Pad {

		#region Protected
		protected override void TakeAction(float offset) {
			var gameplaySettings = GameSettings.HurdlingGameSettings.HurdlingGameplaySettings;
			var amount = gameplaySettings.AccelerationLayout.Evaluate(offset);
			((HurdlingPlayer) Player).Accelerate(amount);
		}
		#endregion
	}
}
